<?php namespace Fenix440\Model\Description\Exceptions;

/**
 * Invalid Description Exception
 *
 * Throws this exception when invalid Model description has been provided
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Description\Exceptions
 */
class InvalidDescriptionException extends \InvalidArgumentException{

}