<?php namespace Fenix440\Model\Description\Interfaces;

use Fenix440\Model\Description\Exceptions\InvalidDescriptionException;

/**
 * Interface Description Aware
 *
 * A component/resource must be aware of "Description"
 *
 * Components, classes or objects that implements this interface, promise that a given value can be set, get and validated.
 * Furthermore, depending upon implementation, a default value might be returned, if no value has been set prior to obtaining it.
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Description\Interfaces
 */
interface DescriptionAware {


    /**
     * Set this component description
     *
     * @param string $description               Description
     * @return void
     * @throws InvalidDescriptionException      If given description is invalid
     */
    public function setDescription($description);

    /**
     * Get this component description
     *
     * @see DescriptionAware::getDefaultDescription()
     * @see DescriptionAware::setDescription($description)
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Validates if this component description is valid
     *
     * @param string $description               Description
     * @return boolean                          <b>True</b> if description is valid, otherwise
     *                                          <b>false</b>
     */
    public function isDescriptionValid($description);

    /**
     * Get this component default description
     *
     * @return string|null
     */
    public function getDefaultDescription();

    /**
     * Check if this component has set description
     * @return bool                             <b>true</b> if component has set description,
     *                                          <b>false</b> otherwise
     */
    public function hasDescription();

    /**
     * Check if there is a default description, which can be used if no description has been
     * set
     *
     * @return bool                             <b>true</b> if there is a default description available
     *                                          <b>false</b> if not
     */
    public function hasDefaultDescription();




}