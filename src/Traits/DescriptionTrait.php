<?php namespace Fenix440\Model\Description\Traits;

use Fenix440\Model\Description\Exceptions\InvalidDescriptionException;
use Aedart\Validate\String\TextValidator;

/**
 * Trait Description
 *
 * @see DescriptionAware
 * @see http://php.net/manual/en/language.types.string.php
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Description\Traits
 */
trait DescriptionTrait {

    /**
     * Description for this component
     *
     * @var null|string
     */
    protected $description=null;


    /**
     * Set this component description
     *
     * @param string $description               Description
     * @return void
     * @throws InvalidDescriptionException      If given description is invalid
     */
    public function setDescription($description){
        if(!$this->isDescriptionValid($description))
            throw new InvalidDescriptionException(sprintf('Description "%s" is invalid',$description));
        $this->description=(string)$description;
    }

    /**
     * Get this component description
     *
     * @see DescriptionAware::getDefaultDescription()
     * @see DescriptionAware::setDescription($description)
     *
     * @return string|null
     */
    public function getDescription(){
        if(!$this->hasDescription() && $this->hasDefaultDescription()){
            $this->setDescription($this->getDefaultDescription());
        }
        return $this->description;
    }

    /**
     * Validates if this component description is valid
     *
     * @param string $description               Description
     * @return boolean                          <b>True</b> if description is valid, otherwise
     *                                          <b>false</b>
     */
    public function isDescriptionValid($description){
        return TextValidator::isValid($description);
    }

    /**
     * Get this component default description
     *
     * @return string|null
     */
    public function getDefaultDescription(){
        return null;
    }

    /**
     * Check if this component has set description
     * @return bool                             <b>true</b> if component has set description,
     *                                          <b>false</b> otherwise
     */
    public function hasDescription(){
        if(!is_null($this->description))
            return true;
        return false;
    }

    /**
     * Check if there is a default description, which can be used if no description has been
     * set
     *
     * @return bool                             <b>true</b> if there is a default description available
     *                                          <b>false</b> if not
     */
    public function hasDefaultDescription(){
        return (!is_null($this->getDefaultDescription()))? true:false;
    }


}