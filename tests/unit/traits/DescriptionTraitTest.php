<?php

use Fenix440\Model\Description\Traits\DescriptionTrait;

/**
 * Class DescriptionTraitTest
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @coversDefaultClass Fenix440\Model\Description\Traits\DescriptionTrait
 */
class DescriptionTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Description\Interfaces\DescriptionAware
     */
    protected function getTraitMock()
    {
        $m = $this->getMockForTrait('Fenix440\Model\Description\Traits\DescriptionTrait');
        return $m;
    }

    /**
     * Get a dummy object of that trait
     * @return DummyDescription
     */
    protected function getDummyObj(){
        return new DummyDescription();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers  ::getDefaultDescription
     * @covers  ::setDescription
     * @covers  ::isDescriptionValid
     * @covers  ::getDescription
     * @covers  ::hasDescription
     */
    public function getDefaultDescription()
    {
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getDescription());
    }

    /**
     * @test
     * @covers  ::setDescription
     * @covers  ::getDescription
     * @covers  ::isDescriptionValid
     * @covers  ::hasDescription
     * @covers  ::hasDefaultDescription
     */
    public function setAndGetDescription()
    {
        $trait = $this->getTraitMock();
        $description = "Don't we love, unit testing!!!";
        $trait->setDescription($description);

        $this->assertSame($description, $trait->getDescription());
    }

    /**
     * @test
     * @covers  ::setDescription
     * @covers  ::isDescriptionValid
     * @expectedException \Fenix440\Model\Description\Exceptions\InvalidDescriptionException
     */
    public function setInvalidDescription()
    {
        $trait = $this->getTraitMock();
        $description = (int)100000;

        $trait->setDescription($description);
    }

    /**
     * @test
     *
     * @covers ::setDescription
     * @covers ::getDescription
     * @covers ::isDescriptionValid
     * @covers ::hasDescription
     * @covers ::hasDefaultDescription
     * @covers ::getDefaultDescription
     */
    public function getDescriptionFromCustomDefault(){
        $dummy = $this->getDummyObj();
        $dummy->defaultDescription="This is my default description";

        $this->assertSame($dummy->defaultDescription, $dummy->getDescription());
    }


}

/**
 * Class DummyDescription
 *
 */
class DummyDescription
{

    use DescriptionTrait;

    public $defaultDescription=null;

    public function getDefaultDescription(){
        return $this->defaultDescription;
    }
}